package com.md.training.repository;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.md.training.domain.employee;
@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository{

	
	
	private String INSERTEMPLOYEE="INSERT INTO EMPLOYEE(EMPID, NAME, AGE, SALARY, DESIGNATION, DEPARTMENT, DEPT_ID) VALUES (?,?,?,?,?,?,?)";
	private String INSERTDEPARTMENT="INSERT INTO DEPARTMENT(DEPT_ID, DEPARTMENT) VALUES (?,?)";
	
	
	private JdbcTemplate jdbcTemplate;
	
	
	@Autowired
	public void setDataSource(DataSource ds){
		jdbcTemplate = new JdbcTemplate(ds);
	}
	


	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	
	
	
	
	public void addEmployee(employee e) {
		
		
		jdbcTemplate.update(INSERTDEPARTMENT, e.getDeptid(),e.getDepartment());
		jdbcTemplate.update(INSERTEMPLOYEE, e.getEmpid(),e.getName(),e.getAge(),e.getSalary(),e.getDesignation(),e.getDepartment(),e.getDeptid());
		
		
	}

}
