package com.md.training.test;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.md.training.domain.employee;
import com.md.training.service.EmployeeService;


@Controller
@RequestMapping("/hello")
public class test {
	
	@Autowired
	private EmployeeService employeeService;
	
	
	public EmployeeService getEmployeeService() {
		return employeeService;
	}

	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@RequestMapping("/addemployee")
	public String addnewEmployee(Model model)
	{
		model.addAttribute("employeedetails",new employee());
		return "addemployee";
	}
	
	@RequestMapping(value="/addemployee", method=RequestMethod.POST)
	public String register(@Valid @ModelAttribute("addemployee") employee e, BindingResult result, Model model)
	{
		
		employeeService.addEmployee(e);
		
		model.addAttribute("e",e.getName());
		
		return "display";
	}
	
	

}
