<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
	<title>Add Employee</title>
</head>
<body>
<h2>Add Employee</h2>
<form:form method="post" modelAttribute="employeedetails" >

	<table>
	<tr>

		<td>Employee ID</td>
		<td><form:input path="empid" /></td>
		<td> <form:errors path="empid"/> </td>
	</tr>
	<tr>
		<td> Name</td>
		<td><form:input path="name" /></td>
		<td> <form:errors path="name"/> </td>
	</tr>
	<tr>
		<td> Age</td>
		<td><form:input path="age" /></td>
		<td> <form:errors path="age"/> </td>
	</tr>
	<tr>
		<td> Salary</td>
		<td><form:input path="salary" /></td>
		<td> <form:errors path="salary"/> </td>
	</tr>
	<tr>
		<td> Designation</td>
		<td><form:input path="designation" /></td>
		<td> <form:errors path="designation"/> </td>
	</tr>
	<tr>
		<td> Department ID</td>
		<td><form:input path="deptid" /></td>
		<td> <form:errors path="deptid"/> </td>
	</tr>
	<tr>
		<td> Department</td>
		<td><form:input path="department" /></td>
		<td> <form:errors path="department"/> </td>
	</tr>
	
	
	
		<tr>
		<td colspan="3">
			<input type="submit" value="Add"/>
		</td>
		
	</tr>
</table>	

</form:form>
</body>
</html>